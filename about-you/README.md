

In your heart you're a superhero. Your mission is to save the world - or a paricular community (in IRL or online) - from an injustice or an unnecessarily painful experience. You're determined to make the world a better place, even when most others in your circle don't see what you see. 

When you switch on your superhero identify your superpowers are: grit, focus, iteration, curiosity and empathy.

Yes of course, I've worked with everyday superheros to identify and solve routine problems. 
