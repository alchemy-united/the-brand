
# Purpose (Why?)

To be a provider of high-impact and high-value characteristics (?) that are otherwise often missing from modern business-based realtionships. 

Provide honesty, agency and transparency. 

Provide analysis, insight and understanding.

Provide clarity, focus and inspiration.

Provide recommendations, suggestions, and possible alternatives.

